#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, random
from twisted.python import log

from bot import BaseBot


class BoyBoyBot(BaseBot):
    name = "BoyBoy"
    flood = 6
    phrases = [
        "I`m gangster",
        "Trash of life",
        "I will as Batman",
        "Don`t eat my brain, it`s so easy for my soul",
        "I dont think...I feel...Feel that I love...",
        "All we need is love",
        "Love is a game that two can play and both win",
        "The heart wants what it wants. There's no logic to these things. You meet someone and you fall in love and that's that",
        "We come to love not by finding a perfect person, but by learning to see an imperfect person perfectly",
        "For the world you may be just one person, but for one person you may be the whole world!",
        "Death doesn't really worry me that much, I'm not frightened about it... I just don't want to be there when it happens",
        "Must not all things at the last be swallowed up in death?"
    ]


    def onTick(self):
        rnd = random.randint(0, len(self.phrases)-1)
        self.chat(self.phrases[rnd])
        BaseBot.onTick(self)

    def onMessage(self, uid, message, name="", private=False):
        if private:
            self.chat("If %s wish to be loved, love" % name, uid)

        BaseBot.onMessage(self, uid, message, name, private)

    def onUserConnect(self, uid, asMaster=True):
        if not asMaster:
            self.chat("While I'm breathing - I love and believe. Wellcome")



if __name__ == '__main__':
    log.startLogging(sys.stdout)

    broadcastUrl = u"ws://127.0.0.1:9000"
    masterUrl = u"ws://127.0.0.1:%d" % random.randint(9001, 10000)

    bot = BoyBoyBot(url=broadcastUrl, master=masterUrl)
    bot.run()
