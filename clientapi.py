# -*- coding: utf-8 -*-

import json


class BaseProtocol():
    """
    Base request-response protocol
    """

    def __init__(self):
        pass

    def apply(self, u):
        raise Exception("Must be implemented")


class RequestFactory():
    """
    Request factory
    """

    @staticmethod
    def create(payload, isBinary):
        if not isBinary:
            params = json.loads(payload)
            type = params.get('type')

            if type == 'auth':
                return AuthRequest(params)
            elif type == 'message':
                return MessageRequest(params)
            elif type == 'file_send_request':
                return FileSendRequest(params)
            elif type == 'file_send_accept':
                return FileSendAcceptRequest(params)
        else:
            return FileRequest(payload)

        return None


class ResponseFactory():
    """
    Response factory
    """

    @staticmethod
    def create(type, isBinary, **params):
        if not isBinary:
            if type == 'auth':
                return AuthResponse(**params)
            elif type == 'message':
                return MessageResponse(**params)
            elif type == 'file_send_request':
                return FileSendResponse(**params)
            elif type == 'file_send_accept':
                return FileSendAcceptResponse(**params)
        else:
            if type == 'file':
                return FileResponse(**params)

        return None


class AuthRequest(BaseProtocol):
    """
    Auth request
    """
    uid = None

    def __init__(self, request):
        self.uid = request.get('uid')
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.auth(self.uid)


class MessageRequest(BaseProtocol):
    """
    Message request
    """
    message = None
    name = ''
    private = False

    def __init__(self, request):
        self.message = request.get('message')
        self.name = request.get('name', '')
        self.private = request.get('private', False)
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.message(self.message, self.name, self.private)


class FileSendRequest(BaseProtocol):
    """
    FileSend request
    """
    filename = None

    def __init__(self, request):
        self.filename = request.get('filename')
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.file_send_request(self.filename)

class FileSendAcceptRequest(BaseProtocol):
    """
    FileSend request
    """
    filename = None
    accept = False

    def __init__(self, request):
        self.filename = request.get('filename')
        self.accept = request.get('accept', False)
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.file_send_accept(self.filename, self.accept)


class FileRequest(BaseProtocol):
    """
    File request
    """
    file = None

    def __init__(self, file):
        self.file = file
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.file(self.file)


class AuthResponse(BaseProtocol):
    """
    Auth response
    """
    uid = None

    def __init__(self, uid=None):
        self.uid = uid
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.sendMessage(json.dumps({
            'type': 'auth',
            'uid': self.uid
        }))


class MessageResponse(BaseProtocol):
    """
    Message response
    """
    message = None
    name = ""
    private = False

    def __init__(self, message=None, name="", private=False):
        self.message = message
        self.name = name
        self.private = private
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.sendMessage(json.dumps({
            'type': 'message',
            'message': self.message,
            'name': self.name,
            'private': self.private
        }))


class FileSendResponse(BaseProtocol):
    """
    FileSend response
    """
    filename = None

    def __init__(self, filename=None):
        self.filename = filename
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.sendMessage(json.dumps({
            'type': 'file_send_request',
            'filename': self.filename
        }))

class FileSendAcceptResponse(BaseProtocol):
    """
    FileSend response
    """
    accept = False
    filename = None

    def __init__(self, filename, accept):
        self.filename = filename
        self.accept = accept
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.sendMessage(json.dumps({
            'type': 'file_send_accept',
            'accept': self.accept,
            'filename': self.filename
        }))


class FileResponse(BaseProtocol):
    """
    File response
    """
    file = None

    def __init__(self, file=None):
        self.file = file
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.sendMessage(self.file, True)
