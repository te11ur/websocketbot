#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, random
from twisted.python import log

from bot import BaseBot


class AlisaBot(BaseBot):
    name = "Alisa"
    flood = 5
    phrases = [
        "La la la, Alisa is cool girl",
        "Hmmmm...",
        "Rain...",
        "Do u know about new album of Prodigy?",
        "Oh Prague is so cool!",
        "I have some crazy story about our chat:)"
    ]

    def onTick(self):
        rnd = random.randint(0, len(self.phrases) - 1)
        self.chat(self.phrases[rnd])
        BaseBot.onTick(self)

    def onMessage(self, uid, message, name="", private=False):
        if private:
            self.chat("Oh %s speak in Chines pls..." % name, uid)

        BaseBot.onMessage(self, uid, message, name, private)

    def onUserConnect(self, uid, asMaster=True):
        if not asMaster:
            self.chat("Hi, my name Alisa, and u?", uid)

    def onFileSendRequest(self, uid, filename):
        BaseBot.onFileSendRequest(self, uid, filename)
        self.chat("No a don`t want photo!", uid)
        return False



if __name__ == '__main__':
    log.startLogging(sys.stdout)

    broadcastUrl = u"ws://127.0.0.1:9000"
    masterUrl = u"ws://127.0.0.1:%d" % random.randint(9001, 10000)

    bot = AlisaBot(url=broadcastUrl, master=masterUrl)
    bot.run()
