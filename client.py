# -*- coding: utf-8 -*-

import os
from autobahn.twisted.websocket import WebSocketAdapterProtocol, WebSocketClientProtocol, WebSocketClientFactory, \
    WebSocketServerFactory, \
    WebSocketServerProtocol, connectWS, listenWS
from broadcastapi import BroadcastClientRequestFactory, BroadcastClientResponseFactory
from clientapi import RequestFactory, ResponseFactory

dir_path = os.path.dirname(os.path.realpath(__file__))

class BaseProtocol(WebSocketAdapterProtocol):
    master = True
    uid = None
    files_send = {}
    files_receive = {}

    def auth(self, uid):
        self.uid = uid
        self.factory.broadcast.register(self, self.uid)
        self.factory.broadcast.bot.onUserConnect(uid=self.uid, asMaster=self.master)

    def message(self, message, name, private):
        self.factory.broadcast.bot.onMessage(uid=self.uid, message=message, name=name, private=private)

    def file_send_request(self, filename):
        accept = self.factory.broadcast.bot.onFileSendRequest(uid=self.uid, filename=filename)
        if accept:
            if not self.files_receive.has_key(self.uid):
                self.files_receive[self.uid] = []

            files = self.files_receive.get(self.uid)
            files.append(filename)

        response = ResponseFactory.create(type='file_send_accept', isBinary=False, filename=filename, accept=accept)

        if response is not None:
            response.apply(self)

    def file(self, file):
        if self.files_receive.has_key(self.uid):
            files = self.files_receive.get(self.uid)
            filename = files.pop()
            dir = '%s\\%s' % (dir_path, self.uid)
            path = '%s\\%s' % (dir, filename)
            if not os.path.exists(dir):
                os.makedirs(dir)

            if not os.path.exists(path):
                f = open(path, 'wb')
                f.write(file)
                f.close()

    def file_send_accept(self, filename, accept):
        if self.files_send.has_key(self.uid):
            self.factory.broadcast.bot.onFileSend(uid=self.uid, filename=filename, accept=accept)
            if accept:
                files = self.files_send.get(self.uid)
                path = files.get(filename, None)

                if path is not None:
                    del files[filename]

                    f = open(path, 'rb')
                    file = f.read()
                    f.close()

                    response = ResponseFactory.create(type='file', isBinary=True, file=file)

                    if response is not None:
                        response.apply(self)

    def file_send(self, path, filename):
        self.files_send[self.uid] = {filename: path}
        response = ResponseFactory.create(type='file_send_request', isBinary=False, filename=filename)

        if response is not None:
            response.apply(self)

    def chat(self, message, name="", private=False):
        response = ResponseFactory.create(type='message', isBinary=False, message=message, name=name)

        if response is not None:
            response.apply(self)

    def onOpen(self):

        response = ResponseFactory.create(type='auth', isBinary=False, uid=self.factory.uid)

        if response is not None:
            response.apply(self)

    def onMessage(self, payload, isBinary):
        request = RequestFactory.create(payload, isBinary)

        if request is not None:
            request.apply(self)

    def connectionLost(self, reason):
        self.factory.broadcast.unregister(self, self.uid)


class SlaveProtocol(WebSocketClientProtocol, BaseProtocol):
    master = False

    def auth(self, uid):
        BaseProtocol.auth(self, uid)
        print "Connect me(%s) to master(%s)" % (self.factory.uid, self.uid)

    def onOpen(self):
        BaseProtocol.onOpen(self)
        WebSocketClientProtocol.onOpen(self)

    def onMessage(self, payload, isBinary):
        BaseProtocol.onMessage(self, payload, isBinary)
        WebSocketClientProtocol.onMessage(self, payload, isBinary)

    def connectionLost(self, reason):
        BaseProtocol.connectionLost(self, reason)
        print "Disconnect me(%s) from master(%s)" % (self.factory.uid, self.uid)
        WebSocketClientProtocol.connectionLost(self, reason)


class SlaveFactory(WebSocketClientFactory):
    uid = None
    broadcast = None

    def __init__(self, url, broadcast, uid):
        WebSocketClientFactory.__init__(self, url)
        self.uid = uid
        self.broadcast = broadcast


class MasterProtocol(WebSocketServerProtocol, BaseProtocol):
    def auth(self, uid):
        BaseProtocol.auth(self, uid)
        print "Connect slave(%s) to me(%s)" % (self.uid, self.factory.uid)

    def onOpen(self):
        BaseProtocol.onOpen(self)
        WebSocketServerProtocol.onOpen(self)

    def onMessage(self, payload, isBinary):
        BaseProtocol.onMessage(self, payload, isBinary)
        WebSocketServerProtocol.onMessage(self, payload, isBinary)

    def connectionLost(self, reason):
        BaseProtocol.connectionLost(self, reason)
        print "Disconnect slave(%s) from me(%s)" % (self.uid, self.factory.uid)
        WebSocketServerProtocol.connectionLost(self, reason)


class MasterFactory(WebSocketServerFactory):
    uid = None
    broadcast = None

    def __init__(self, url, broadcast, uid):
        self.uid = uid
        self.broadcast = broadcast
        WebSocketServerFactory.__init__(self, url)


class BroadcastProtocol(WebSocketClientProtocol):
    """ Self uid"""
    uid = None

    def auth(self, uid):
        self.uid = uid
        self.factory.register(self, self.uid)
        self.factory.createmaster(self.uid)
        print "self uid: %s" % self.uid

    def users(self, users):
        self.factory.refresh(users)

    def onOpen(self):
        response = BroadcastClientResponseFactory.create(type='auth', isBinary=False, url=self.factory.masterUrl)

        if response is not None:
            response.apply(self)

        WebSocketClientProtocol.onOpen(self)

    def onMessage(self, payload, isBinary):
        request = BroadcastClientRequestFactory.create(payload, isBinary)

        if request is not None:
            request.apply(self)

        WebSocketClientProtocol.onMessage(self, payload, isBinary)

    def connectionLost(self, reason):
        self.factory.unregister(self, self.uid)
        WebSocketClientProtocol.connectionLost(self, reason)


class BroadcastFactory(WebSocketClientFactory):
    """ Self uid"""
    uid = None
    masterUrl = None
    master = None
    users = {}
    user = None
    bot = None

    def __init__(self, broadcast, master, bot):
        self.uid = None
        self.users = {}
        self.master = None
        self.user = None
        self.masterUrl = master
        self.bot = bot
        WebSocketClientFactory.__init__(self, broadcast)

    def createmaster(self, uid):
        factory = MasterFactory(url=self.masterUrl, broadcast=self, uid=uid)
        factory.protocol = MasterProtocol
        listenWS(factory)
        self.master = factory

    def createslave(self, url):
        factory = SlaveFactory(url=url, broadcast=self, uid=self.uid)
        factory.protocol = SlaveProtocol
        connectWS(factory)
        return factory

    def refresh(self, users):
        for u in users:
            uid = u.get('uid')
            url = u.get('url')
            if not self.users.has_key(uid) and url is not None:
                self.createslave(url)

    def register(self, u, uid):

        if (isinstance(u, BroadcastProtocol)):
            self.user = u
            self.uid = uid
            return

        if not self.users.has_key(uid):
            self.users[uid] = {"master": None, "slave": None}

        user = self.users.get(uid)
        if isinstance(u, MasterProtocol):
            user["master"] = u
        elif isinstance(u, SlaveProtocol):
            user["slave"] = u

    def unregister(self, u, uid):
        if (isinstance(u, BroadcastProtocol)):
            self.user = None
            return

        if self.users.has_key(uid):
            user = self.users.get(uid)

            if isinstance(u, MasterProtocol):
                user['master'] = None
            elif isinstance(u, SlaveProtocol):
                user['slave'] = None

            if user.get('master') is None and user.get('slave') is None:
                del self.users[uid]

    def chat(self, message, name, uid=None):
        """
        Chat message to all or private
        @:param message - message
        @:param uid - private message to uid
        """
        for uuid, user in self.users.items():
            master = user.get('master')
            # slave = user.get('slave')
            if master is not None and (uid is None or master.uid == uid):
                master.chat(message, name, uid is None)
            # if slave is not None and (uid is None or slave.uid == uid):
            #    slave.chat(message, self.bot.name, uid is None)

    def file(self, path, filename, uid=None):
        for uuid, user in self.users.items():
            master = user.get('master')
            # slave = user.get('slave')
            if master is not None and (uid is None or master.uid == uid):
                master.file_send(path, filename)
            # if slave is not None and (uid is None or slave.uid == uid):
            #    slave.file_send(path, filename)



def printMessage(uid, message):
    print "%s: %s" % (uid, message)
