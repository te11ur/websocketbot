#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, uuid

from twisted.internet import reactor
from twisted.python import log
from twisted.web.server import Site
from twisted.web.static import File
from broadcastapi import BroadcastServerRequestFactory, BroadcastServerResponseFactory

from autobahn.twisted.websocket import WebSocketServerFactory, WebSocketServerProtocol, listenWS
from autobahn.twisted.resource import WebSocketResource


class BroadcastServerProtocol(WebSocketServerProtocol):
    """
    Server-side instance of client
    """
    uid = None
    url = None

    def auth(self, url=None):
        """
        Called on client auth. Generate uid and send auth data to client
        :param url: address of client
        :return: void
        """
        self.url = url
        self.uid = uuid.uuid4().__str__()
        self.factory.register(self)
        print "New user connected with uid: %s" % self.uid

        response = BroadcastServerResponseFactory.create(type='auth', isBinary=False, uid=self.uid)

        if response is not None:
            response.apply(self)

    def users(self, users):
        response = BroadcastServerResponseFactory.create(type='users', isBinary=False, users=users)
        if response is not None:
            response.apply(self)

    def onMessage(self, payload, isBinary):
        request = BroadcastServerRequestFactory.create(payload, isBinary)

        if request is not None:
            request.apply(self)

        WebSocketServerProtocol.onMessage(self, payload, isBinary)

    def connectionLost(self, reason):
        self.factory.unregister(self)
        print "Disconnect user with uid: %s" % self.uid
        WebSocketServerProtocol.connectionLost(self, reason)

    def to_dict(self):
        return {
            "uid": self.uid,
            "url": self.url
        }


class BroadcastServerFactory(WebSocketServerFactory):
    """
    Server instance
    """
    users = []

    def __init__(self, url):
        WebSocketServerFactory.__init__(self, url)
        self.users = []
        self.tick()

    def user(self, peer):
        """
        Generate user list and exclude client with called peer
        :param peer: client peer
        :return: list
        """
        return [u.to_dict() for u in self.users if u.peer != peer]

    def tick(self):
        """
        Sending to all clients actial information about connected clients right now
        :return: void
        """
        for u in self.users:
            u.users(self.user(u.peer))

        reactor.callLater(2, self.tick)

    def register(self, u):
        """
        Register client in client the list
        :param u:
        :return: void
        """
        if u not in self.users:
            self.users.append(u)

    def unregister(self, u):
        """
        Unregister client from the list
        :param u:
        :return: void
        """
        if u in self.users:
            self.users.remove(u)


if __name__ == '__main__':
    log.startLogging(sys.stdout)

    factory = BroadcastServerFactory(url=u"ws://127.0.0.1:9000")
    factory.protocol = BroadcastServerProtocol
    listenWS(factory)

    webdir = File(".")
    #resource = WebSocketResource(factory)
    # websockets resource on "/ws" path
    #webdir.putChild(u"ws", resource)

    web = Site(webdir)
    reactor.listenTCP(8080, web)

    reactor.run()
