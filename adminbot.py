#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, random, os
from twisted.python import log
from bot import BaseBot

dir_path = os.path.dirname(os.path.realpath(__file__))


class AdminBot(BaseBot):
    name = "Admin"
    flood = 10
    phrases = [
        "Don`t flood",
        "Hey, do u talk about u`r travel?",
        "Don`t stop",
        "Ooo, i`v some cookie",
        "I love Paris",
        "Do u remember my photo from Italy?"
    ]
    files = [
        'test.txt',
        'test2.txt',
        'test3.txt'
    ]

    def onTick(self):
        rnd = random.randint(0, len(self.files) - 1)
        filename = self.files[rnd]
        path = "%s\\%s" % (dir_path, filename)
        self.file(path, filename)
        BaseBot.onTick(self)

    def onMessage(self, uid, message, name="", private=False):
        if private:
            self.chat("%s What?" % name, uid)
        else:
            rnd = random.randint(0, len(self.phrases) - 1)
            self.chat(self.phrases[rnd])

        BaseBot.onMessage(self, uid, message, name, private)

    def onUserConnect(self, uid, asMaster=True):
        if not asMaster:
            self.chat("U`r wellcome %s!" % uid)

    def onFileSend(self, uid, filename, accept):
        if not accept:
            self.chat("Why u reject my photo %s?" % filename, uid)


if __name__ == '__main__':
    log.startLogging(sys.stdout)

    broadcastUrl = u"ws://127.0.0.1:9000"
    masterUrl = u"ws://127.0.0.1:%d" % random.randint(9001, 10000)

    bot = AdminBot(url=broadcastUrl, master=masterUrl)
    bot.run()
