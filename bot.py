# -*- coding: utf-8 -*-

from client import BroadcastFactory, BroadcastProtocol
from autobahn.twisted.websocket import connectWS
from twisted.internet import reactor


class BaseBot():
    name = ""
    flood = 10
    broadcast = None

    def __init__(self, url, master):
        self.broadcast = BroadcastFactory(broadcast=url, master=master, bot=self)
        self.broadcast.protocol = BroadcastProtocol
        connectWS(self.broadcast)

    def chat(self, message, uid=None):
        if self.isReady():
            self.broadcast.chat(message, self.name, uid)

    def file(self, path, filename, uid=None):
        if self.isReady():
            self.broadcast.file(path, filename, uid)

    def run(self):
        self.onTick()
        reactor.run()

    def isReady(self):
        return self.broadcast.master is not None

    def onTick(self):
        reactor.callLater(self.flood, self.onTick)

    def onMessage(self, uid, message, name="", private=False):
        if private:
            print "%s(private)(%s): %s" % (uid, name, message)
            return

        print "%s(%s): %s" % (uid, name, message)

    def onUserConnect(self, uid, asMaster=True):
        pass

    def onFileSend(self, uid, filename, accept):
        pass

    def onFileSendRequest(self, uid, filename):
        print "%s want send u '%s' file" % (uid, filename)
        return True
