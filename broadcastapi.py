# -*- coding: utf-8 -*-

import json


class BaseProtocol():
    """
    Base request-response protocol
    """

    def __init__(self):
        pass

    def apply(self, u):
        raise Exception("Must be implemented")


class BroadcastServerRequestFactory():
    """
    Broadcast server side request factory
    """

    @staticmethod
    def create(payload, isBinary):
        if not isBinary:
            params = json.loads(payload)
            type = params.get('type')

            if type == 'auth':
                return BroadcastServerAuthRequest(params)

        return None


class BroadcastServerResponseFactory():
    """
    Broadcast server side response factory
    """

    @staticmethod
    def create(type, isBinary, **params):
        if not isBinary:
            if type == 'auth':
                return BroadcastServerAuthResponse(**params)
            elif type == 'users':
                return BroadcastServerUsersResponse(**params)

        return None


class BroadcastClientRequestFactory():
    """
    Broadcast client side request factory
    """

    @staticmethod
    def create(payload, isBinary):
        if not isBinary:
            params = json.loads(payload)
            type = params.get('type')

            if type == 'auth':
                return BroadcastClientAuthRequest(params)
            elif type == 'users':
                return BroadcastClientUsersRequest(params)

        return None


class BroadcastClientResponseFactory():
    """
    Broadcast client side response factory
    """

    @staticmethod
    def create(type, isBinary, **params):
        if not isBinary:
            if type == 'auth':
                return BroadcastClientAuthResponse(**params)

        return None


class BroadcastClientAuthRequest(BaseProtocol):
    """
    Client side Auth request
    """
    uid = None

    def __init__(self, request):
        self.uid = request.get('uid')
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.auth(self.uid)


class BroadcastClientUsersRequest(BaseProtocol):
    """
    Client side User list request
    """
    users = None

    def __init__(self, request):
        self.users = request.get('users')
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.users(self.users)


class BroadcastClientAuthResponse(BaseProtocol):
    """
    Client side Auth response
    """
    url = None

    def __init__(self, url=None):
        self.url = url
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.sendMessage(json.dumps({
            'type': 'auth',
            'url': self.url
        }))


class BroadcastServerAuthRequest(BaseProtocol):
    """
    Server side Auth request
    """
    url = None

    def __init__(self, request):
        self.url = request.get('url')
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.auth(self.url)


class BroadcastServerAuthResponse(BaseProtocol):
    """
    Server side Auth response
    """
    uid = None

    def __init__(self, uid):
        self.uid = uid
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.sendMessage(json.dumps({
            'type': 'auth',
            'uid': self.uid
        }))


class BroadcastServerUsersResponse(BaseProtocol):
    """
    Server side User list response,
    """
    users = None

    def __init__(self, users):
        self.users = users
        BaseProtocol.__init__(self)

    def apply(self, u):
        u.sendMessage(json.dumps({
            'type': 'users',
            'users': self.users
        }))
